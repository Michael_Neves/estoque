<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Estoque;

class Fornecedor extends Model {

    protected $table = 'fornecedor';

    protected $fillable = ['id', 'nome'];
}
