<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model {
    
    protected $table = 'usuario';

    protected $primaryKey = 'id';
    protected $fillable = ['id', 'login', 'name', 'password'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}
