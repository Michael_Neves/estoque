<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Estoque;

class Produto extends Model {
    
    protected $table = 'produto';
    protected $fillable = ['id', 'estoque', 'preco', 'descricao'];
}
