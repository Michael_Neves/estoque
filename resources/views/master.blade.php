<!DOCTYPE html>
<html>
    <head>
        <title>Gallery Application in AngularJS</title>
        <link rel="stylesheet" href="{{ asset('lib/css/bootstrap.css') }}">
    </head>
    <body ng-app="App">
        <div class="container">
            <div ui-view></div>
        </div>

        <script src="{{ asset('node_modules/angular/angular.js') }}"></script>
        <script src="{{ asset('node_modules/angular-ui-router/release/angular-ui-router.js') }}"></script>
        <script src="{{ asset('node_modules/angular-cookies/angular-cookies.js') }}"></script>

        <script src="{{ asset('js/app.min.js') }}"></script>
        <script src="{{ asset('js/controller/controller.min.js') }}"></script>
    </body>
</html>