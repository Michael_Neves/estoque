<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Login</title>
    
    <link rel="stylesheet" href="./css/app.css">
    <link rel="stylesheet" href="./css/login.css">
</head>
<body>
    <div class="container">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
        <div class="row">
            <div class="col-xs-12">
                <div class="panel-login row">
                    <div class="col-xs-12 text-center margin-bottom">
                        <h3 class="title green-color">
                            Sistema Estoque
                        </h3>

                        <span class="sub-title">
                            Seja Bem Vindo
                        </span>
                    </div>
                    
                    {{ Form::open([ 'route' => 'signIn.post', 'method' => 'POST'])}}
                    
                        <div class="col-xs-12 form-group">
                            <label>Login</label>
                            <input
                                type="text"
                                class="form-control"
                                name="login">
                        </div>

                        <div class="col-xs-12 form-group">
                            <label>Senha</label>
                            <input
                                type="password"
                                class="form-control"
                                name="password">
                        </div>

                        <div class="col-xs-12 form-group">
                            <button
                                type="submit"
                                class="btn btn-primary btn-block">
                                Entrar
                            </button>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</body>
</html>