<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Login</title>
    
    <link rel="stylesheet" href="./css/app.css">
    <link rel="stylesheet" href="./css/login.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel-login row">
                    <div class="col-xs-12 text-center margin-bottom">
                        <h3 class="title green-color">
                            Sistema Estoque
                        </h3>

                        <span class="sub-title">
                            Seja Bem Vindo
                        </span>
                    </div>

                    {{ Form::open([ 'route' => 'register.post', 'method' => 'POST'])}}

                        <div class="col-xs-12 form-group">
                            <label>Nome</label>
                            <input
                                type="text"
                                class="form-control"
                                name="name">
                        </div>

                        <div class="col-xs-12 form-group">
                            <label>Login</label>
                            <input
                            type="text"
                            class="form-control"
                            name="login">
                        </div>

                        <div class="col-xs-12 form-group">
                            <label>Senha</label>
                            <input
                                type="text"
                                class="form-control"
                                name="password">
                        </div>

                        <div class="col-xs-12 form-group">
                            <button class="btn btn-primary btn-block">
                                Entrar
                            </button>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</body>
</html>